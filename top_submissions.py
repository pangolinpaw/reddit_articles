import os
from datetime import datetime
import csv
import argparse
import red

REDDIT = red.auth()

# Function: save_csv()
#
# Save a 2-dimensional list to a CSV file
#
# Parameters:
#
#   filename - Path & name of file to which data will be saved
#   data - 2D list of data to save
#
def save_csv(filename, data):
    with open(filename, 'a', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerows(data)

# Function: main()
#
# Main loop that fetches data from Reddit and saves the content in CSV format.
#
# Parameters:
#
#   subreddit - Subreddit name minus `/r/`, e.g. "python"
#   save_as - Full path & .xlsx file name to which content will be saved
#   limit - Maximum number of submissions to extract
#   comment_limit - Maximum number of comments. Defaults to 3 pages' worth, or fetches all if set to 0
#
def main(subreddit, save_as, limit, comment_limit=None):
    print(' Fetching submissions from Reddit...')
    if comment_limit == 0:
        all_comments = True
    else:
        all_comments = False
    submissions = red.get_sub_content(REDDIT, subreddit, limit, all_comments)
    print(' Formatting for output...')
    output = [['TITLE', 'AUTHOR', 'BODY', 'COMMENTS']]
    for counter, submission in enumerate(submissions):
        comments = []
        comment_list = submission['comments']
        if comment_limit is not None:
            comment_list = comment_list[:comment_limit]
        for comment in comment_list:
            if comment['replies'] != []:
                reply_string = '\n'.join([f"    > {reply['author']['name']}: {reply['body']}" for reply in comment['replies']])
            else:
                reply_string = ''
            comment_string = f"{comment['author']['name']}: {comment['body']}\n{reply_string}"
            comments.append(comment_string)
        row = [
            submission['title'],
            submission['author'],
            submission['body'],
            '\n'.join(comments)
        ]

        output.append(row)
        if counter % 5 == 0:
            save_csv(save_as, output)
            output = []
    save_csv(save_as, output)
    if len(save_as) > 50:
        save_as = f'...{save_as[-50:]}'
    print(f' Done! Saved output to {save_as}')
    print(' ============================')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scheduled Subreddit Scraping')
    parser.add_argument('sub', help='subreddit name minus `/r/`, e.g. "python"')
    parser.add_argument('--save_as', help='full path & .csv file name to which content will be saved')
    parser.add_argument('--limit', help='max. number of submissions to include (default = 10)')
    parser.add_argument('--comment_limit', help='max. number of comments (default = 3 pages, set an integer limit or 0 for all)')
    args = parser.parse_args()
    if args.save_as is None:
        dirname, scriptname = os.path.split(os.path.abspath(__file__))
        save_as = f'{dirname}{os.sep}{args.sub} ({datetime.now().strftime("%Y-%m-%d")}).csv'
    else:
        save_as = args.save_as
    if args.limit is None:
        limit = 10
    else:
        limit = int(args.limit)

    if args.comment_limit is not None:
        comment_limit = int(args.comment_limit)
    else:
        comment_limit = None

    print('\n Scheduled Subreddit Scraping')
    print(' ============================')
    main(args.sub, save_as, limit, comment_limit)
