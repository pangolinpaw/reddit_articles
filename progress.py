# Function: update()
#
# Displays an ASCII progress bar for long-running processes.
#
# Parameters:
#
#   current - Current item number
#   maximim - Total items to process
#   message - Optional. Message to display alongside progress bar
#
def update(current, maximum, message=None):
    width = 2
    padding = 50
    perc = round((float(current) / maximum) * 100, 1)
    rounded_perc = int(round(perc,0) / width)
    bar_string = rounded_perc * '#'
    bar_string = bar_string.ljust(padding, '-')
    if message is None:
        bar_string = '[{}] {}%'.format(bar_string, str(int(perc)).ljust(3))
    else:
        if len(message) > 50:
            message = f'{message[:50]}...'
        bar_string = '[{}] {}% | {}'.format(bar_string, str(int(perc)).ljust(3), message.ljust(55))
    print ('\r   {}'.format(bar_string), end='')
