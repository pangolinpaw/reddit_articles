# Scheduled Subreddit Scraping

## Installation

This application requires Python 3.0 or above be installed on the host system. This is available from https://www.python.org/downloads/

Download and extract this repository, then install requirements with:

`pip install -r requirements.txt`

## Usage

This application is designed to be run on a regular schedule (e.g. once a day). It can be run from the command line and/or controlled via a Cronjob with the following command:

`python3 top_submissions.py [subreddit] [--save_as filename] [--limit x] [--all_comments]`

Where:

 - `subreddit` is the name of the subreddit to scrape
 - `filename` in `--save_as filename` is the (optional) full path to and name of the CSV file to which data should be saved. If none is specified, the data will be saved in a file in the current folder.
 - The `x` in `--limit x` is the (optional) number of submissions to include in the scrape (default = 10)
 - Including the `--all_comments` flag will include all comments with each submission, instead of the default maximum of 3 pages.

While running, a bar will indicate the script's progress as it fetches and formats data from the Subreddit. 

## Documentation

Full documentation can be found in the documentation folder of this repository, courtesy of [Natural Docs](https://naturaldocs.org/)

