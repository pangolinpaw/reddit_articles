import os
import json
import praw
import progress
from unidecode import unidecode

dirname, scriptname = os.path.split(os.path.abspath(__file__))
THIS_DIRECTORY = f'{dirname}{os.sep}'

# Function: get_creds()
#
# Reads saved API credentials from an `auth.json` file.
#
# Authorisation File Format:
#
# === code ===
#   {
#       "username": "Reddit_Username",
#       "password": "account_password",
#       "client_id": "API_ID",
#       "client_secret": "API_key"
#   }
# ============
#
# See Also:
# 
#   - <auth()>
#
def get_creds():
    with open(f'{THIS_DIRECTORY}auth.json', 'r', encoding='utf-8') as f:
        data = json.loads(f.read())
        return data

# Function: auth()
#
# Uses the credentials from <get_creds()> to authenticate the script with the Reddit API.
#
# See Also:
# 
#   - <get_creds()>
#
def auth():
    creds = get_creds()
    reddit = praw.Reddit(
        client_id=creds['client_id'],
        client_secret=creds['client_secret'],
        password=creds['password'],
        user_agent="red_cmd by u/PangolinPaws",
        username=creds['username'],
    )
    return reddit

# Function: save_json()
#
# Reusable JSON file reading funciton.
#
# Parameters:
#
#   filename - full path to and name of JSON file
#   data - list, dictionary or string to save
#
def save_json(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4))

# Function: get_sub_content()
#
# Accesses and extracts content from the latest submissions to the specified subreddit.
#
# Parameters:
#
#   reddit - authenticated reddit instance (see <auth()>)
#   subreddit - name of subreddit to access (without any leading '/r/')
#   limit - Optional. Returns content from this number of submissions from the specified subreddit
#
# Returns:
#
# A list of dictionaries, one for each submission. E.g.:
#
# === code ===
#   [
#    {
#        "title": "Submission title",
#        "author": "author_name",
#        "body": "Submission body, empty string for a link post",
#        "comments": [
#            {
#                "author": {
#                    "id": "xxxxxx",
#                    "name": "comment_author_name"
#                },
#                "body": "Comment body",
#                "replies": []
#            },
#            ...
#        ]
#    },
#    ...
#  ]
# ============
def get_sub_content(reddit, subreddit, limit=10, all_comments=False):
    output = []
    subreddit = reddit.subreddit(subreddit)
    for counter, submission in enumerate(subreddit.new()):
        progress.update(counter, limit, submission.title)
        sub_content = {}
        sub_content['title'] = unidecode(submission.title)
        sub_content['author'] = submission.author.name
        if submission.selftext == '': # link posts have an empty string body
            sub_content['body'] = submission.url
        else:
            sub_content['body'] = unidecode(submission.selftext)
        sub_content['comments'] = []
        if all_comments:
            comment_pages = 0
        else:
            comment_pages = 3
        submission.comments.replace_more(limit=comment_pages) # https://praw.readthedocs.io/en/stable/tutorials/comments.html#the-replace-more-method
        for top_level_comment in submission.comments:
            top_level_replies = []
            for second_level_comment in top_level_comment.replies:
                second_level_replies = []
                for third_level_comment in second_level_comment.replies:
                    author = third_level_comment.author
                    try:
                        author = {
                            'id':author.id,
                            'name':author.name
                        }
                    except AttributeError:
                        author = {
                            'id':None,
                            'name':None
                        }
                    second_level_replies.append({
                        'author':author,
                        'body':unidecode(third_level_comment.body)
                    })
                author = second_level_comment.author
                try:
                    author = {
                        'id':author.id,
                        'name':author.name
                    }
                except AttributeError:
                    author = {
                        'id':None,
                        'name':None
                    }
                top_level_replies.append({
                    'author':author,
                    'body':unidecode(second_level_comment.body),
                    'replies':second_level_replies
                })
            author = top_level_comment.author
            try:
                author = {
                    'id':author.id,
                    'name':author.name
                }
            except AttributeError:
                author = {
                    'id':None,
                    'name':None
                }
            sub_content['comments'].append({
                'author':author,
                'body':unidecode(top_level_comment.body),
                'replies':top_level_replies
            })
        output.append(sub_content)
        if counter >= limit -1:
            break
    progress.update(1, 1)
    print()
    return output

def main():
    reddit = auth()
    content = get_sub_content(reddit, 'animals', 3)
    save_json('sample_output.json', content)


if __name__ == '__main__':
    main()
